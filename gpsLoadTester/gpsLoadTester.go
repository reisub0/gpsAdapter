package main

import (
	"fmt"
	"math/rand"
	"net"
	"sync"
	"time"
)

const (
	NoConnections int    = 10000
	server        string = "reisub.ddns.net:8000"
	TIMEFORMAT    string = "150405"
	DATEFORMAT    string = "020106"
	MESSAGE       string = "GTPL $1,867322035135813,A,%s,%s,18.709738,N,80.068397,E,0,406,309,11,0,14,1,0,26.4470#"
)

var limiter chan struct{}
var wg sync.WaitGroup

func MakeConnection() {
	for {
		// time.Sleep(time.Duration(rand.Intn(60)) * time.Second)
		_ = rand.Float64()
		limiter <- struct{}{}
		conn, err := net.Dial("tcp", server)
		for err != nil {
			fmt.Printf(".")
			<-limiter
			return
			// conn, err = net.Dial("tcp", server)
			// fmt.Printf(".")
			// time.Sleep(5 * time.Second)
		}
		conn.Write([]byte(fmt.Sprintf(MESSAGE, time.Now().Format(DATEFORMAT), time.Now().Format(TIMEFORMAT))))
		<-limiter
		// time.Sleep(1 * time.Minute)
	}
}

func main() {
	limiter = make(chan struct{}, 2*NoConnections)
	go func() {
		for {
			fmt.Printf("\nCurrently has %d connections\n", len(limiter))
			time.Sleep(time.Second)
		}
	}()
	wg.Add(NoConnections)
	for i := 0; i < NoConnections; i++ {
		go MakeConnection()
	}
	wg.Wait()
}
